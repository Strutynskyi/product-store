var data;
var xmlhttp = new XMLHttpRequest();
var url = "data.json";

xmlhttp.onloadend = function () {
    if (this.readyState === 4 && this.status === 200) {
        data = JSON.parse(this.responseText);
        renderItems(selectDataByCategory(300))
    }
};
xmlhttp.open("GET", url, true);
xmlhttp.send();

function selectDataByCategory(category) {
    return data.filter(function (item) {
        return item.category === category
    });
}

function formatDate(date) {
    return (new Date(date).getDate() < 9 ? '0' + new Date(date).getDate() : new Date(date).getDate() + '/') +
        (new Date(date).getMonth() + 1 < 9 ? '0' +
            (new Date(date).getMonth() + 1) + '/' : new Date(date).getMonth() + '/') +
        new Date(date).getFullYear()
}

function clearChildNodes(id) {
    const node = document.getElementById(id);
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

function renderItems(items) {
    const parent = document.getElementById('items-container');

    clearChildNodes('items-container');

    items.forEach(function (item) {

        var productItem = document.createElement('div');
        productItem.classList.add('item');

        var productInfo = document.createElement('div');
        productInfo.classList.add('item-info');

        var productName = document.createElement('div');
        productName.classList.add('item-name');
        productName.innerHTML = item.item_name;

        var productDate = document.createElement('div');
        productDate.classList.add('item-date');
        productDate.innerHTML = formatDate(item.created);

        productInfo.appendChild(productName);
        productInfo.appendChild(productDate);

        var productPrice = document.createElement('div');
        productPrice.classList.add('item-price');
        productPrice.innerHTML = item.price;

        productItem.appendChild(productInfo);
        productItem.appendChild(productPrice);

        parent.appendChild(productItem);
    });
}

function switchActiveMenuItem(event, category) {
    var menuItems = document.querySelectorAll(".item-text");
    for (var i = 0; i < menuItems.length; i++) {
        var current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        event.classList.add('active');
    }
    renderItems(selectDataByCategory(category));
}

